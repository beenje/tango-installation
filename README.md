
# Installation of Tango

## What needs to be installed?

* Tango host
  * MariaDB or MySQL database
  * DataBaseds (tango database)
* Tango device server
  * TangoTest device server for testing
  * TangoStarter to run device servers
  * Tango libraries (C++, python, java)
* Tango applications
  * java applications (jive)
  * Tango libraries (C++, python, java)

## Installation from source

TangoSourceDistribution: <https://gitlab.com/tango-controls/TangoSourceDistribution>

Good instructions in the [README](https://gitlab.com/tango-controls/TangoSourceDistribution/-/blob/main/assets/README)

* Come with jar files for Java libraries and applications
* Need to install dependencies yourself before compiling (omniORB, zmq, mysql, gcc, java)

## TANGO_HOST

Check is done in this order:

* Linux:
  * `TANGO_HOST` environment variable
  * `$HOME/.tangorc`
  * `/etc/tangorc`
* Windows:
  * `TANGO_HOST` environment variable
  * `%TANGO_HOME%/tangorc`

## CentOS

```bash
sudo yum install -y epel-release
# Create /etc/yum.repos.d/maxiv.repo
sudo nano /etc/yum.repos.d/maxiv.repo
[maxiv-public]
name=MAX IV public RPM Packages - $basearch
baseurl=http://pubrepo.maxiv.lu.se/rpm/el$releasever/$basearch
gpgcheck=0
enabled=1

sudo yum makecache
```

```bash
# Install and start mariadb
sudo yum install -y mariadb-server
sudo systemctl start mariadb
sudo systemctl enable mariadb
# Install tango-db
sudo yum install -y tango-db tango-test
# Create TANGO database
cd /usr/share/tango-db
$ sudo ./create_db.sh
MySQL host name [localhost]:
MySQL admin user name [root]:
MySQL admin password []:
Name of tango MySQL user [tango]:
Password for tango MySQL user [tango]:

Created database 'tango'

Created user 'tango'

Wrote  /etc/sysconfig/tango-db

# Setup TANGO_HOST
$ sudo cat /etc/tangorc
TANGO_HOST=tangobox:10000
# and / or
$ sudo cat /etc/profile.d/tango.sh
export TANGO_HOST=tangobox:10000
```

```bash
sudo systemctl start tango-db
sudo systemctl enable tango-db
$ sudo systemctl status tango-db
● tango-db.service - Tango database server
   Loaded: loaded (/usr/lib/systemd/system/tango-db.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-10-08 08:58:57 UTC; 34s ago
 Main PID: 3088 (DataBaseds)
   CGroup: /system.slice/tango-db.service
           └─3088 /usr/bin/DataBaseds 2 -ORBendPoint giop:tcp:tangobox:10000

Oct 08 08:58:57 tangobox systemd[1]: Started Tango database server.
Oct 08 08:58:57 tangobox DataBaseds[3088]: main(): arrived
Oct 08 08:58:57 tangobox DataBaseds[3088]: main(): export DataBase as named servant (name=database)
Oct 08 08:58:57 tangobox DataBaseds[3088]: Ready to accept request

$ TangoTest test
Ready to accept request
```

```bash
$ yum list installed | grep libtango
libtango9.x86_64                    9.3.4-21.el7.maxlab         @maxiv-public
# pytango:
sudo yum install -y python36-pytango
# Java applications
sudo yum install -y tango-java
jive
```

## Debian & Ubuntu

```bash
$ sudo apt update
$ sudo apt install mariadb-server
$ sudo apt install tango-db tango-test
$ sudo systemctl status tango-db
● tango-db.service - LSB: Start the tango control system database daemon
   Loaded: loaded (/etc/init.d/tango-db; generated)
   Active: active (running) since Thu 2021-10-07 14:36:47 UTC; 10min ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 9 (limit: 1151)
   CGroup: /system.slice/tango-db.service
           └─4839 /usr/lib/tango/DataBaseds 2 -ORBendPoint giop:tcp::10000

Oct 07 14:36:46 ubuntu-bionic systemd[1]: Starting LSB: Start the tango control system database daemon...
Oct 07 14:36:47 ubuntu-bionic systemd[1]: Started LSB: Start the tango control system database daemon.

$ cat /etc/tangorc
# Config file for my package
TANGO_HOST=ubuntu-bionic:10000

$ /usr/lib/tango/TangoTest test
Ready to accept request
```

```bash
# pytango
sudo apt install python3-tango
# Java applications
$ apt list libtango9
libtango9/bionic,now 9.2.5a+dfsg1-2build1 amd64 [installed,automatic]
sudo apt-get install openjdk-8-jre
curl -LO https://people.debian.org/~picca/libtango-java_9.2.5a-1_all.deb
sudo apt install ./libtango-java_9.2.5a-1_all.deb
```

WARNING! Do not use java 11!

```bash
$ java -version
openjdk version "11.0.11" 2021-04-20
OpenJDK Runtime Environment (build 11.0.11+9-Ubuntu-0ubuntu2.18.04)
OpenJDK 64-Bit Server VM (build 11.0.11+9-Ubuntu-0ubuntu2.18.04, mixed mode, sharing)

$ sudo update-alternatives --list java
/usr/lib/jvm/java-11-openjdk-amd64/bin/java
/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
$ sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
$ java -version
openjdk version "1.8.0_292"
OpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~18.04-b10)
OpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)
```

## Conda

Packages on conda-forge:

* tango-idl
* cpptango
* tango-database
* tango-test
* tango-admin
* pytango
* itango

PyTango available on Windows from the tango-controls channel

[Miniforge](https://github.com/conda-forge/miniforge)
provides conda installer already configured to use conda-forge.
Can even include [mamba](https://github.com/mamba-org/mamba).
Alternative to [Miniconda](https://conda.io/en/latest/miniconda.html).

```bash
# Conda installation
$ curl -LO https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge-Linux-x86_64.sh
$ bash Miniforge-Linux-x86_64.sh -bp $HOME/miniconda
$ source $HOME/miniconda/bin/activate
$ conda init

$ conda config --show channels
channels:
  - conda-forge

$ conda create -y -n tango tango-test python=3.9 pytango
$ conda activate tango
```

## Docker

Works on Linux, macOS and Windows

```yaml
---
version: '2'
services:
  tango-db:
    image: tangocs/mysql:9.2.2
    environment:
     - MYSQL_ROOT_PASSWORD=root
  tango-cs:
    image: tangocs/tango-cs:9
    ports:
     - "10000:10000"
    environment:
     - TANGO_HOST=localhost:10000
     - MYSQL_HOST=tango-db:3306
     - MYSQL_USER=tango
     - MYSQL_PASSWORD=tango
     - MYSQL_DATABASE=tango
    depends_on:
     - tango-db
```

tango-cs running DataBaseds, TangoAccessControl, TangoTest

Other images from SKA: <https://gitlab.com/ska-telescope/ska-tango-images>

## Windows

TangoSetup: Get TangoSetup-9.2.2_win64.exe from <https://www.tango-controls.org/downloads/>

<https://tango-controls.readthedocs.io/en/latest/installation/tango-on-windows.html>


PyTango:

* pip (wheel for windows)
* conda (from tango-controls channel): `conda create -y -n tango -c tango-controls pytango`

## TangoBox

TangoBox is a VM image running Tango Controls system and its various tools. It is intended to be used for demonstration and training.

<https://tango-controls.readthedocs.io/en/latest/installation/vm/tangobox.html>
